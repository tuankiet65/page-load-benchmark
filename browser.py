#!/usr/bin/env python3

import base64
from typing import Optional, List

from google.server import Server
from google.client.chromedriver import ChromeDriver

class Browser:
    def __init__(self,
                 name: str, path: str, chromedriver: str,
                 args: Optional[List] = None,
                 extensions: Optional[List] = None):
        if args is None:
            args = []

        if extensions is None:
            extensions = []

        self.name = name
        self.path = path
        self.chromedriver = Server(chromedriver)
        self.args = args
        self.extensions = self._load_extension(extensions)

    def _load_extension(self, extensions):
        result = []
        for f in extensions:
            content = open(f, "rb").read()
            result.append(base64.b64encode(content).decode())

        return result

    def open(self):
        self.browser = ChromeDriver(
            server_url = self.chromedriver.GetUrl(),
            chrome_binary = self.path,
            chrome_switches = self.args,
            chrome_extensions = self.extensions)

    def load_and_measure(self, url):
        self.browser.Load(url)

        data = self.browser.ExecuteScript("return performance.timing")
        del data['toJSON']

        return data

    def quit(self):
        self.browser.Quit()
