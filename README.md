# page-load-benchmark

Quick and dirty tool for measuring page load speed in Chromium-based browsers
(e.g: Chrome, Yandex, Coc Coc, Vivaldi, Brave, ...) using Chromedriver.

## Motivation
During my internship at [Coc Coc](https://coccoc.com), I need a quick tool to
compare Coc Coc's page load speed to other browsers (notably Chrome).

## How it works
The user specifies the browsers, URLs and number of tries. Then for each
(browser, URL, load interation) combination, Chromedriver is used to
programatically open the browser, load the URL, and collect the load metrics
using the JavaScript `performance.timing` variable. Then the load time is
calculated to be equal to:
`performance.timing.loadEventEnd - performance.timing.domainLookupStart`.
Each load is conducted in a brand new browser instance and profile, without any
leftover caches from previous loads.

## Results
For each run, the results are written into a summary and a detailed file inside
the `result\` directory. The summary file only contains the minimum data for
quick judgement of the load speed:
(browser name, URL, load count, load time as computed from the formula above).
The detailed file contains everything that `performance.timing`
returns for each load to allow for more detailed analysys. Both are in .csv
format.

## Requirements
* Windows (haven't tested on Linux and Mac yet)
* Python 3.5+, no external dependencies needed

## How to use
* Copy config-sample.json to config.json and add your target browsers, URLs,
and tries amount
* Run benchmark.py: `python benchmark.py [path to the config file, if not
specified then config.json in the current directory is used]`

## config.json
### General
* `browsers`: list of `Browser` object, browsers to be tested
* `urls`: list of string, URLs to be loaded
* `tries`: int, how many times the (browser, URL) combination should be repeated

### Browser object
* `name`: string, browser name, used in the reports
* `path`: string, path to the browser's executable file
* `chromedriver`: string, path to the Chromedriver executable.
The Chromedriver specified must match the Chromium version the browser is based
on. For example: Chrome 75 is based on Chromium 75, and the Chromedriver used
must be 75. Coc Coc 80 is based on Chromium 74, and the Chromedriver used
must be 74. Chromedriver can be downloaded here: http://chromedriver.chromium.org
* `extensions`: list of string, path to extensions (.crx file) to be loaded to
the browser before testing
* `args`: list of string, command line arguments to be passed to the browser.
For example, while testing Coc Coc, `enable-load-extension-from-commandline` is
needed because Chromedriver uses the `--load-extension` arg to load extensions
specified above to the browser, and without the
`enable-load-extension-from-commandline` arg, Coc Coc will filter out all
`--load-extension` args (This is actually a security feature, since native apps
can abuse this to install extensions into the browser, for example Norton)

## Acknowledgement
Python code inside `google\` are taken from the Chromium project, which is
licensed under the BSD license, and converted to Python 3-compatible code
using 2to3.

## License
* Source code in the `google\` directory: BSD License
* Everything else: Public domain
