import csv
import logging
import sys
import datetime
import json

from browser import Browser

def current_datetime():
    return datetime.datetime.now().strftime("%Y%m%d-%H%M%S")

# https://www.w3.org/TR/navigation-timing/#processing-model
CSV_FIELDS_SUMMARY = [
    "browser",
    "url",
    "load_id",
    "load_time",
]

CSV_FIELDS_DETAILED = [
    "browser",
    "url",
    "load_id",
    'unloadEventStart',
    'domLoading',
    'fetchStart',
    'responseStart',
    'loadEventEnd',
    'connectStart',
    'domainLookupStart',
    'redirectStart',
    'domContentLoadedEventEnd',
    'requestStart',
    'secureConnectionStart',
    'connectEnd',
    'navigationStart',
    'loadEventStart',
    'domInteractive',
    'domContentLoadedEventStart',
    'redirectEnd',
    'domainLookupEnd',
    'unloadEventEnd',
    'responseEnd',
    'domComplete'
]

logging.basicConfig()
logger = logging.getLogger()
logger.setLevel(logging.INFO)

config_file = "config.json"
if len(sys.argv) > 1:
    if sys.argv[1] in ["-h", "--help"]:
        print(f"Usage: {sys.argv[0]} [path to config file, default: config.json]")
        exit()
    else:
        config_file = sys.argv[1]
with open(config_file) as f:
    config = json.load(f)

summary_csv_file = open(f"result/benchmark-{current_datetime()}-summary.csv", "w")
summary_csv = csv.DictWriter(summary_csv_file,
                             CSV_FIELDS_SUMMARY,
                             lineterminator='\n',
                             extrasaction='ignore')
summary_csv.writeheader()

detailed_csv_file = open(f"result/benchmark-{current_datetime()}-detailed.csv", "w")
detailed_csv = csv.DictWriter(detailed_csv_file,
                              CSV_FIELDS_DETAILED,
                              lineterminator='\n',
                              extrasaction='ignore')
detailed_csv.writeheader()

for browser_json in config["browsers"]:
    browser = Browser(**browser_json)
    for i in range(config["tries"]):
        for url in config["urls"]:
            logger.info(f"Browser: {browser.name}, tries: {i}, URL: {url}")

            browser.open()
            metrics = browser.load_and_measure(url)
            browser.quit()

            metrics["browser"] = browser.name
            metrics["url"] = url
            metrics["load_id"] = i
            metrics["load_time"] = (metrics['loadEventEnd'] - metrics['domainLookupStart']) / 1000.0

            summary_csv.writerow(metrics)
            detailed_csv.writerow(metrics)

summary_csv_file.close()
detailed_csv_file.close()
